#!/usr/bin/env python

from setuptools import setup

setup(
    name="update-hotglue-models",
    version="0.0.3",
    description="update hygraph models",
    author="hotglue",
    url="https://hotglue.xyz",
    classifiers=["Programming Language :: Python :: 3 :: Only"],
    install_requires=["pydantic>=1.9.0", "jsonref==1.0.1", "requests==2.24.0"],
    packages=["update_hotglue_models"],
)
