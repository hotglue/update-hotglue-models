import inspect, os, sys, requests
from importlib.machinery import SourceFileLoader

def replace_any_of(properties):
    if not isinstance(properties, dict):
        return properties

    props = properties.copy()

    if "anyOf" not in props:
        return props

    any_of_list = props["anyOf"]

    if not isinstance(any_of_list, list) or len(any_of_list) != 2:
        return props

    null_type_index = next((i for i, prop in enumerate(any_of_list) if prop.get("type") == "null"), None)

    if null_type_index is None:
        return props

    non_null_type_index = 0 if null_type_index == 1 else 1

    non_null_props = any_of_list[non_null_type_index]

    props.pop("anyOf")

    props.update(non_null_props)

    return props

def replace_ref(properties, definitions):
    for k, v in properties.items():
        value = replace_any_of(v)

        if isinstance(value, dict) and "$ref" in value:
            ref_name = value["$ref"].split("/")[-1]

            properties[k] = definitions[ref_name]
        else:
            if isinstance(value, dict):
                properties[k] = replace_ref(value, definitions)

        if value and isinstance(value, dict) and "items" in value and "properties" in value["items"]:
            value["items"]["properties"] = replace_ref(value["items"]["properties"], definitions)

    return properties


def run():
    try:
        print("Executing HyGraph schemas update...")

        ci_project_dir = os.environ["CI_PROJECT_DIR"]
        file_path = os.environ["FILE_PATH"]
        hygraph_category = os.environ["HYGRAPH_CATEGORY"]
        hygraph_query_url = os.environ["HYGRAPH_QUERY_URL"]
        hygraph_mutation_url = os.environ["HYGRAPH_MUTATION_URL"]
        hygraph_token = os.environ["HYGRAPH_TOKEN"]

        file_path = f"{ci_project_dir}/{file_path}"

        SourceFileLoader("models", file_path).load_module()

        cls_schemas = [
            # jsonref.loads(cls_obj.schema_json())
            {
                "schema": cls_obj.schema(),
                "schema_name": cls_obj.schema_name if hasattr(cls_obj, "schema_name") else None
            }
            for cls_name, cls_obj in inspect.getmembers(sys.modules["models"])
            if inspect.isclass(cls_obj) and cls_obj.__module__ == "models"
        ]

        print("# Schemas found:", len(cls_schemas))

        result = {
            cls_schema["schema_name"]: {
                "properties": replace_ref(
                    cls_schema["schema"]["properties"],
                    cls_schema["schema"].get("definitions") or cls_schema["schema"].get("$defs") or None
                )
            }
            for cls_schema in cls_schemas
            if cls_schema["schema_name"] and cls_schema["schema"].get("type") == "object"
        }

        print("Schemas to update:", list(result.keys()))

        for (model_name, model_properties) in result.items():
            payload = {
                "query": """
                query Assets($name: String, $category: String) {
                    models(
                        where: {
                            developerName: $name,
                            category: {
                                AND: {
                                    _search: $category
                                }
                            }
                        }
                    ) {
                        id
                        name
                    }
                }
                """,
                "variables": {
                    "name": model_name,
                    "category": hygraph_category
                }
            }

            print("Querying for", model_name)

            response = requests.post(hygraph_query_url, json=payload)

            models = response.json()["data"]["models"]

            if models:
                model = models[0]

                model_id = model["id"]

                print("Model found")

                payload = {
                    "query": "mutation($id: ID, $properties: Json) { updateModel( where: { id: $id } data: { properties: $properties } ) { id name } }",
                    "variables": {
                        "id": model_id,
                        "properties": model_properties,
                    }
                }

                headers = {"Authorization" : f"Bearer {hygraph_token}"}

                print("Mutation for", model_name, "with properties", model_properties)

                response = requests.post(hygraph_mutation_url, json=payload, headers=headers)

                print("response:", response.status_code, response.text)

                payload = {
                    "query": """mutation($id: ID) {
                        publishModel(where: { id: $id }, to: PUBLISHED) {
                            id
                        }
                    }""",
                    "variables": {"id": model_id},
                }

                print("Publishing model", model_name, "of id", model_id)

                response = requests.post(hygraph_mutation_url, json=payload, headers=headers)

                print("response:", response.status_code, response.text)
            else:
                print("Model not found")
    except Exception as e:
        print("An error occurred:", str(e))
